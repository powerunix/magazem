// Url del backend
const iduser = 132509398;
//Recurso de la lista de los ids

let idsurl = `https://api.mercadolibre.com/users/${iduser}/items/search?status=active&limit=100&attributes=results&access_token=${localStorage.getItem(
  "token"
)}`;

//Fetch para obtener la lista de los ids
fetch(idsurl)
  .then((response) => response.json())
  .then((x) => localStorage.setItem("claves", x.results));

//Es el id html de donde aparecen los productos
const mainProductos = document.getElementById("ListaProductos");
/* mainProductos.innerHTML = "<p>Loading..."; */
//Obtiene el item de los IDS del Almacenamiento del navegador
let g = localStorage.getItem("claves");
//Separa el array con comas
let x = g.split(",");

//Esta promesa hace la magia de separar url por url mostrando url por uno
var lapromesa = Promise.all(
  x.map((id) =>
    fetch(
      `https://api.mercadolibre.com/items?ids=${id}&access_token=${localStorage.getItem(
        "token"
      )}`
    ).then((resp) => resp.json())
  )
).then((nueva) => funcion(nueva));

let funcion = (nueva) => {
  //Filtra los datos que deben salir
  nueva.forEach((element) => {
    let titulo = element.map((x) => x.body.title);
    let precio = element.map((x) => x.body.price);
    /* let cantidad = element.map(x => x.body.initial_quantity-x.body.sold_quantity)  */
    let cantidad = element.map((x) => x.body.available_quantity);
    let imagen = element.map((x) => x.body.secure_thumbnail);
    let id = element.map((x) => x.body.id);
    let stock = element.map((x) => x.body.inventory_id);
    let enlace = element.map((x) => x.body.permalink);
    //Limpia los datos de carteres
    titulo = JSON.stringify(titulo)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    precio = JSON.stringify(precio)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    cantidad = JSON.stringify(cantidad)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    id = JSON.stringify(id)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    imagen = JSON.stringify(imagen)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    stock = JSON.stringify(stock)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    enlace = JSON.stringify(enlace)
      .replace(/['"]+/g, "")
      .replace(/^\[(.+)\]$/, "$1");
    //Muestra los productos

    mainProductos.innerHTML += `
    

    <tr>
      <th scope="row"><a target="_blank" href="${enlace}">${id}</a></th>
      <td>${titulo}</td>
      <td><img src="${imagen}"></td>
      <td>$${[precio]}</td>
      <td class="cantidad">${cantidad}</td>
    </tr>


    
    `;
    document.getElementById("totaldeproductos").innerHTML = nueva.length;
  });
};
