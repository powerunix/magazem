using System;
using System.Collections.Generic;
using System.Linq;
using MeliLibTools.Client;
using MeliLibTools.MeliLibApi;
using MeliLibTools.Model;
using MercadoLibre.SDK;
using MercadoLibre.SDK.Meta;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace Magazen
{

    [ApiController]
    [Route("/posts")]

    public class InicioController : Controller
    {

        private readonly IConfiguration _config;

        public InicioController(IConfiguration config)
        {
            _config = config;
        }
        //El resultado de los ids
        public class ItemsID
        {
            public string id { get; set; }
            public string results { get; set; }
            public string body { get; set; }

        }

        //Obtiene el token de mercadolibre
        [Authorize(Roles = "Administrador")]
        [HttpGet("obtener")]
        public ActionResult<List<String>> ObtieneToken()
        {
            string _magazenApiKey = _config["Magazen:clienteSecreto"];
            string _magazenID = _config["Magazen:IdCliente"];
            string _urlglobal = _config["Magazen:urlglobal"];
            Console.WriteLine(_magazenApiKey);
            var m = new MeliApiService
            {
                Credentials = new MeliCredentials(MeliSite.Mexico, long.Parse(_magazenID), _magazenApiKey)
            };
            var redirectUrl = m.GetAuthUrl(long.Parse(_magazenID), MeliSite.Argentina, _urlglobal + "/posts/");
            m.AuthorizeAsync("the received code", _urlglobal + "/posts/");
            Console.WriteLine(m.Credentials.ClientId);


            return Redirect(redirectUrl);
        }

        //Página de redireccion 

        [HttpGet]
        public ActionResult<ItemsID> AccesoPrincipal()
        {
            // string 
            Configuration config = new Configuration();

            string _magazenApiKey = _config["Magazen:clienteSecreto"];
            string _magazenID = _config["Magazen:IdCliente"];
            string _urlglobal = _config["Magazen:urlglobal"];

            var apiInstancItem = new ItemsApi(config);
            var apiInstance = new OAuth20Api(config);


            var grantType = "authorization_code";
            var clientId = _magazenID;
            var clientSecret = _magazenApiKey;  // string
            var redirectUri = _urlglobal + "/posts/";  // string 
            string page = HttpContext.Request.Query["code"].ToString();
            var code = page;


            try
            {

                // Request Access Token
                Token result = apiInstance.GetToken(grantType, clientId, clientSecret, redirectUri, code);
                Console.WriteLine(result.AccessToken);

                // Return a Item.
                string llave = result.AccessToken;
                ViewBag.token = result.AccessToken;
                string miurl = "https://api.mercadolibre.com/users/" + _magazenID + "/items/search?status=active&access_token=" + llave;
                var client = new RestClient(miurl);
                var request = new RestRequest("/", Method.GET);
                var queryResult = client.Execute<List<ItemsID>>(request).Data;
                var consulta = queryResult.Select(x => x).ToList();
                var miresultado = JsonConvert.SerializeObject(consulta);

                return View();
                /* return Ok(apiInstancItem.ItemsIdGetWithHttpInfo(id).Data.ToString()); */

            }
            catch (ApiException e)
            {
                Console.WriteLine("Exception when calling ItemsApi.ItemsIdGet: " + e.Message);
                Console.WriteLine("Status Code: " + e.ErrorCode);
                Console.WriteLine(e.StackTrace);
            }

            return Ok("Error");

        }
        [Authorize(Roles = "Administrador")]

        [HttpGet("listado")]
        public IActionResult ListaProductos()
        {
            return View();
        }




    }

}
